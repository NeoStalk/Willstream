<?php

function operators_data_save ($query_name, &$context) {

  $limit = 10;

  $query_results = mssql_import($query_name);

  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($query_results);
  }

  if (empty($context['sandbox']['items'])) {
    $context['sandbox']['items'] = $query_results;
  }

  $operations = array_splice($context['sandbox']['items'], 0, $limit);
  foreach ($operations as $data) {

    $date = variable_get('Request_Date');
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->propertyCondition('type', 'operator')
      ->fieldCondition('field_id', 'value', $data->Operator_Id, '=');
    $query_result = $query->execute();

    if ($query_result) {
      $query_result = array_shift($query_result);
      $node_result = array_shift($query_result);
      $node_entity = node_load($node_result->nid);
    }
    else {
      $values = array(
        'type' => 'operator',
        'status' => 1,
      );
      $node_entity = entity_create('node', $values);
    }

    traffic_data_save($node_entity, $data, $date);

    $context['sandbox']['progress']++;
    $context['message'] = t('Обрабатывается запись %progress из %count', array('%progress' => $context['sandbox']['progress'], '%count' => $context['sandbox']['max']));
    $context['results']['processed'] = $context['sandbox']['progress'];
  }
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}


function traffic_data_save($node_entity, $data, $date) {

  $position = strripos($data->Operator_Name, ']');
  if ($position) {
    $operator_name = substr($data->Operator_Name, $position + 1);
    $div_name = substr($data->Operator_Name, 0, $position + 1);
  }
  else {
    $operator_name = $data->Operator_Name;
    $div_name = 'Без направления';
  }

  $node_wrapper = entity_metadata_wrapper('node', $node_entity);
  $node_wrapper->title = $operator_name;
  $node_wrapper->field_id = $data->Operator_Id;

  $vocabulary_name = 'division';
  $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_name);
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'taxonomy_term')
    ->propertyCondition('vid', $vocabulary->vid)
    ->propertyCondition('name', $div_name, '=');

  $result = $query->execute();

  if (count($result) > 0) {
    $result_array = array_shift($result);
    $tid_obj = array_shift($result_array);
    $tid = $tid_obj->tid;
    $term = taxonomy_term_load($tid);
  }
  else {
    $term = entity_create('taxonomy_term', array('vid' => $vocabulary->vid, 'name' => $div_name));
    taxonomy_term_save($term);
  }

  $term_wrapper = entity_metadata_wrapper('taxonomy_term', $term);
  $node_wrapper->field_division = $term_wrapper->tid->value();
  $node_wrapper->save();

  $query =
         "SELECT field_data_field_operator_traffic.field_operator_traffic_value
          FROM  field_data_field_operator_traffic, field_collection_item, field_data_field_date
          WHERE field_data_field_operator_traffic.entity_id = " . $node_entity->nid . " " .
         "AND field_data_field_operator_traffic.field_operator_traffic_value  = field_data_field_date.entity_id
          AND field_data_field_operator_traffic.field_operator_traffic_value = field_collection_item.item_id
          AND DATE(field_data_field_date.field_date_value) = " . "'" . $date . "'";
  $query_result = db_query($query)->fetchAll();

  if (count($query_result) > 0) {
    $query_result = array_shift($query_result);
    $fc_entity = entity_load_single('field_collection_item', $query_result->field_operator_traffic_value);
  }
  else {
    $fc_entity = entity_create('field_collection_item', array('field_name' => 'field_operator_traffic'));
    $fc_entity->setHostEntity('node', $node_entity);
  }
 
  $fc_entity_wrapper = entity_metadata_wrapper('field_collection_item', $fc_entity);
 
  $fc_entity_wrapper->field_date = strtotime($date);
 
  if (isset($data->Operator_State_Code)) {
    if ($data->Operator_State_Code == 7) {
      $fc_entity_wrapper->field_pcp = ceil($data->Time);
    }
    if ($data->Operator_State_Code == 9) {
      $fc_entity_wrapper->field_rest = ceil($data->Time);
    }
  }
  if (isset($data->Work_Time)) {
    $fc_entity_wrapper->field_logged = ceil($data->Work_Time);
  }

  if (isset($data->Traffic)) {
    $fc_entity_wrapper->field_traffic = ceil($data->Traffic);
  }

  if (isset($data->Operator_Calls)) {
    $fc_entity_wrapper->field_number_calls = $data->Operator_Calls;
  }


  $fc_entity_wrapper->save();
  field_attach_update('node', $node_entity);
  watchdog('operators_data_import', 'saved' . $node_entity->title);

}

function operators_data_import_finished_callback($success, $results, $operations) {

  if ($success) {
    $message = format_plural($results['processed'], 'One node processed.', '@count nodes processed.');
  }
  else {
    $message = t('Finished with an error.');
  }
  drupal_set_message($message);

}

function mssql_import($query_name) {


  $date = variable_get('Request_Date');
  db_set_active('external');
  if ($query_name == 'rest_pcp') {
    $query =
            " SELECT DISTINCT T1.Id AS Operator_Id, T2.State AS Operator_State_Code, T1.Name AS Operator_Name, SUM(T2.LenTime) AS Time
              FROM oktell_cc_temp.dbo.A_Cube_CC_Cat_OperatorInfo AS T1
              INNER JOIN oktell_cc_temp.dbo.A_Cube_CC_OperatorStates AS T2
              ON T1.Id = T2.IdOperator 
              AND CAST(T2.DateTimeStop AS DATE) =  " . "'" . $date . "'" .
            " AND (T2.State = 7 OR T2.State = 9) 
              GROUP BY T1.Id, T2.State, T1.Name";
  }

  if ($query_name == 'work_time') {
    $query =
            "SELECT DISTINCT T1.Id AS Operator_Id, T1.Name AS Operator_Name,  SUM(T2.LenTime) AS Work_Time
             FROM oktell_cc_temp.dbo.A_Cube_CC_Cat_OperatorInfo AS T1
             INNER JOIN oktell_cc_temp.dbo.A_Cube_CC_OperatorStates AS T2
             ON T1.Id = T2.IdOperator 
             AND CAST(T2.DateTimeStop AS DATE) = " . "'" . $date . "'" .
            "GROUP BY T1.Id, T1.Name";
  }

  if ($query_name == 'traffic') {
      $query =
          " SELECT A_Cube_CC_Cat_OperatorInfo.Id AS Operator_Id, A_Cube_CC_Cat_OperatorInfo.Name AS Operator_Name, SUM(DATEDIFF(second, A_Cube_CC_EffortConnections.DateTimeStart, A_Cube_CC_EffortConnections.DateTimeStop)) AS Traffic,
            COUNT(A_Cube_CC_EffortConnections.IdEffort) as Operator_Calls
            FROM  A_Cube_CC_Cat_OperatorInfo, A_Cube_CC_EffortConnections
            WHERE A_Cube_CC_Cat_OperatorInfo.Id = A_Cube_CC_EffortConnections.IdOperator 
            AND CAST(A_Cube_CC_EffortConnections.DateTimeStop AS DATE) =" . "'" . $date . "'" .
          " GROUP BY A_Cube_CC_Cat_OperatorInfo.Id, A_Cube_CC_Cat_OperatorInfo.Name";


  }
  $result = db_query($query)->fetchAll();
  db_set_active();

  return $result;
}
