<?php
/**
 * @file
 * custom_module_access.features.inc
 */

/**
 * Implements hook_views_api().
 */
function custom_module_access_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
