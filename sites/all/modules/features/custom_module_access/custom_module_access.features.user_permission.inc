<?php
/**
 * @file
 * custom_module_access.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function custom_module_access_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'operators_data_import page access'.
  $permissions['operators_data_import page access'] = array(
    'name' => 'operators_data_import page access',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'operators_data_import',
  );

  // Exported permission: 'records_operators_calls page access'.
  $permissions['records_operators_calls page access'] = array(
    'name' => 'records_operators_calls page access',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'records_operators_calls',
  );

  return $permissions;
}
