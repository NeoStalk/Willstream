<?php
/**
 * @file
 * custom_module_access.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function custom_module_access_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'rest_and_pcp';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Трафик по операторам';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Трафик';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'ещё';
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
    2 => '2',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'input_required';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Применить';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Сбросить';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Сортировать по';
  $handler->display->display_options['exposed_form']['options']['sort_asc_label'] = 'По возрастанию';
  $handler->display->display_options['exposed_form']['options']['sort_desc_label'] = 'По убыванию';
  $handler->display->display_options['exposed_form']['options']['text_input_required'] = 'Выберите любой фильтр и нажмите Применить для просмотра результата';
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'filtered_html';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_input' => 'field_input',
    'field_output' => 'field_output',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_input' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_output' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Связь: Содержимое: Трафик (field_operator_traffic) */
  $handler->display->display_options['relationships']['field_operator_traffic_value']['id'] = 'field_operator_traffic_value';
  $handler->display->display_options['relationships']['field_operator_traffic_value']['table'] = 'field_data_field_operator_traffic';
  $handler->display->display_options['relationships']['field_operator_traffic_value']['field'] = 'field_operator_traffic_value';
  $handler->display->display_options['relationships']['field_operator_traffic_value']['required'] = TRUE;
  $handler->display->display_options['relationships']['field_operator_traffic_value']['delta'] = '-1';
  /* Поле: Элемент коллекции полей: Дата */
  $handler->display->display_options['fields']['field_date']['id'] = 'field_date';
  $handler->display->display_options['fields']['field_date']['table'] = 'field_data_field_date';
  $handler->display->display_options['fields']['field_date']['field'] = 'field_date';
  $handler->display->display_options['fields']['field_date']['relationship'] = 'field_operator_traffic_value';
  $handler->display->display_options['fields']['field_date']['settings'] = array(
    'format_type' => 'custom',
    'custom_date_format' => 'd-m-Y',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
    'show_repeat_rule' => 'show',
  );
  /* Поле: Содержимое: Направление */
  $handler->display->display_options['fields']['field_division']['id'] = 'field_division';
  $handler->display->display_options['fields']['field_division']['table'] = 'field_data_field_division';
  $handler->display->display_options['fields']['field_division']['field'] = 'field_division';
  $handler->display->display_options['fields']['field_division']['type'] = 'taxonomy_term_reference_plain';
  /* Поле: Содержимое: Заголовок */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Ф.И.О. оператора';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Поле: Элемент коллекции полей: Logged */
  $handler->display->display_options['fields']['field_logged']['id'] = 'field_logged';
  $handler->display->display_options['fields']['field_logged']['table'] = 'field_data_field_logged';
  $handler->display->display_options['fields']['field_logged']['field'] = 'field_logged';
  $handler->display->display_options['fields']['field_logged']['relationship'] = 'field_operator_traffic_value';
  $handler->display->display_options['fields']['field_logged']['exclude'] = TRUE;
  /* Поле: Глобальный: PHP */
  $handler->display->display_options['fields']['php_1']['id'] = 'php_1';
  $handler->display->display_options['fields']['php_1']['table'] = 'views';
  $handler->display->display_options['fields']['php_1']['field'] = 'php';
  $handler->display->display_options['fields']['php_1']['label'] = 'Logged';
  $handler->display->display_options['fields']['php_1']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php_1']['php_output'] = '<?php
print  gmdate("H:i:s", $row->field_logged);

?>
';
  $handler->display->display_options['fields']['php_1']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php_1']['php_click_sortable'] = '';
  /* Поле: Элемент коллекции полей: Перерыв */
  $handler->display->display_options['fields']['field_rest']['id'] = 'field_rest';
  $handler->display->display_options['fields']['field_rest']['table'] = 'field_data_field_rest';
  $handler->display->display_options['fields']['field_rest']['field'] = 'field_rest';
  $handler->display->display_options['fields']['field_rest']['relationship'] = 'field_operator_traffic_value';
  $handler->display->display_options['fields']['field_rest']['exclude'] = TRUE;
  /* Поле: Глобальный: PHP */
  $handler->display->display_options['fields']['php_2']['id'] = 'php_2';
  $handler->display->display_options['fields']['php_2']['table'] = 'views';
  $handler->display->display_options['fields']['php_2']['field'] = 'php';
  $handler->display->display_options['fields']['php_2']['label'] = 'Перерыв';
  $handler->display->display_options['fields']['php_2']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php_2']['php_output'] = '<?php
print gmdate("H:i:s", $row->field_rest);
?>';
  $handler->display->display_options['fields']['php_2']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php_2']['php_click_sortable'] = '';
  /* Поле: Глобальный: PHP */
  $handler->display->display_options['fields']['php_5']['id'] = 'php_5';
  $handler->display->display_options['fields']['php_5']['table'] = 'views';
  $handler->display->display_options['fields']['php_5']['field'] = 'php';
  $handler->display->display_options['fields']['php_5']['label'] = '% перерыва к отработанному времени';
  $handler->display->display_options['fields']['php_5']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php_5']['php_output'] = '<?php
if ($row->field_logged !=0 and !empty($row->field_logged)) {
print round(($row->field_rest/$row->field_logged)*100, 2);
}
?>';
  $handler->display->display_options['fields']['php_5']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php_5']['php_click_sortable'] = '';
  /* Поле: Элемент коллекции полей: Поствызывная обработка */
  $handler->display->display_options['fields']['field_pcp']['id'] = 'field_pcp';
  $handler->display->display_options['fields']['field_pcp']['table'] = 'field_data_field_pcp';
  $handler->display->display_options['fields']['field_pcp']['field'] = 'field_pcp';
  $handler->display->display_options['fields']['field_pcp']['relationship'] = 'field_operator_traffic_value';
  $handler->display->display_options['fields']['field_pcp']['exclude'] = TRUE;
  /* Поле: Глобальный: PHP */
  $handler->display->display_options['fields']['php_3']['id'] = 'php_3';
  $handler->display->display_options['fields']['php_3']['table'] = 'views';
  $handler->display->display_options['fields']['php_3']['field'] = 'php';
  $handler->display->display_options['fields']['php_3']['label'] = 'Поствызывная обработка';
  $handler->display->display_options['fields']['php_3']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php_3']['php_output'] = '<?php
print gmdate("H:i:s",$row->field_pcp);
?>';
  $handler->display->display_options['fields']['php_3']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php_3']['php_click_sortable'] = '';
  /* Поле: Глобальный: PHP */
  $handler->display->display_options['fields']['php_6']['id'] = 'php_6';
  $handler->display->display_options['fields']['php_6']['table'] = 'views';
  $handler->display->display_options['fields']['php_6']['field'] = 'php';
  $handler->display->display_options['fields']['php_6']['label'] = '% поствызова к отработанному времени';
  $handler->display->display_options['fields']['php_6']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php_6']['php_output'] = '<?php
if ($row->field_logged !=0 and !empty($row->field_logged)) {
print round(($row->field_pcp/$row->field_logged)*100, 2);
}
?>';
  $handler->display->display_options['fields']['php_6']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php_6']['php_click_sortable'] = '';
  /* Поле: Элемент коллекции полей: Traffic */
  $handler->display->display_options['fields']['field_traffic']['id'] = 'field_traffic';
  $handler->display->display_options['fields']['field_traffic']['table'] = 'field_data_field_traffic';
  $handler->display->display_options['fields']['field_traffic']['field'] = 'field_traffic';
  $handler->display->display_options['fields']['field_traffic']['relationship'] = 'field_operator_traffic_value';
  $handler->display->display_options['fields']['field_traffic']['label'] = 'Трафик';
  $handler->display->display_options['fields']['field_traffic']['exclude'] = TRUE;
  /* Поле: Глобальный: PHP */
  $handler->display->display_options['fields']['php']['id'] = 'php';
  $handler->display->display_options['fields']['php']['table'] = 'views';
  $handler->display->display_options['fields']['php']['field'] = 'php';
  $handler->display->display_options['fields']['php']['label'] = 'Трафик';
  $handler->display->display_options['fields']['php']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php']['php_output'] = '<?php
print gmdate("H:i:s", $row->field_traffic);
?>';
  $handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php']['php_click_sortable'] = '';
  /* Поле: Глобальный: PHP */
  $handler->display->display_options['fields']['php_4']['id'] = 'php_4';
  $handler->display->display_options['fields']['php_4']['table'] = 'views';
  $handler->display->display_options['fields']['php_4']['field'] = 'php';
  $handler->display->display_options['fields']['php_4']['label'] = '% загрузки';
  $handler->display->display_options['fields']['php_4']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php_4']['php_output'] = '<?php
if ($row->field_logged !=0 and !empty($row->field_logged)) {
print round(($row->field_traffic/$row->field_logged)*100, 2);
}
?>';
  $handler->display->display_options['fields']['php_4']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php_4']['php_click_sortable'] = '';
  /* Поле: Элемент коллекции полей: Количество звонков */
  $handler->display->display_options['fields']['field_number_calls']['id'] = 'field_number_calls';
  $handler->display->display_options['fields']['field_number_calls']['table'] = 'field_data_field_number_calls';
  $handler->display->display_options['fields']['field_number_calls']['field'] = 'field_number_calls';
  $handler->display->display_options['fields']['field_number_calls']['relationship'] = 'field_operator_traffic_value';
  $handler->display->display_options['fields']['field_number_calls']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Поле: Массовые операции: Содержимое */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['row_clickable'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::vbo_export_xlsx_action' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 1,
      'skip_permission_check' => 1,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'strip_tags' => 1,
      ),
    ),
  );
  /* Критерий сортировки: Содержимое: Направление (field_division) */
  $handler->display->display_options['sorts']['field_division_tid']['id'] = 'field_division_tid';
  $handler->display->display_options['sorts']['field_division_tid']['table'] = 'field_data_field_division';
  $handler->display->display_options['sorts']['field_division_tid']['field'] = 'field_division_tid';
  /* Критерий сортировки: Элемент коллекции полей: Дата (field_date) */
  $handler->display->display_options['sorts']['field_date_value']['id'] = 'field_date_value';
  $handler->display->display_options['sorts']['field_date_value']['table'] = 'field_data_field_date';
  $handler->display->display_options['sorts']['field_date_value']['field'] = 'field_date_value';
  $handler->display->display_options['sorts']['field_date_value']['relationship'] = 'field_operator_traffic_value';
  /* Критерий сортировки: Содержимое: Заголовок */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Критерий фильтра: Содержимое: Опубликовано */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Критерий фильтра: Содержимое: Тип */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'operator' => 'operator',
  );
  /* Критерий фильтра: Элемент коллекции полей: Дата (field_date) */
  $handler->display->display_options['filters']['field_date_value']['id'] = 'field_date_value';
  $handler->display->display_options['filters']['field_date_value']['table'] = 'field_data_field_date';
  $handler->display->display_options['filters']['field_date_value']['field'] = 'field_date_value';
  $handler->display->display_options['filters']['field_date_value']['relationship'] = 'field_operator_traffic_value';
  $handler->display->display_options['filters']['field_date_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_date_value']['expose']['operator_id'] = 'field_date_value_op';
  $handler->display->display_options['filters']['field_date_value']['expose']['label'] = 'Дата';
  $handler->display->display_options['filters']['field_date_value']['expose']['operator'] = 'field_date_value_op';
  $handler->display->display_options['filters']['field_date_value']['expose']['identifier'] = 'field_date_value';
  $handler->display->display_options['filters']['field_date_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_date_value']['form_type'] = 'date_popup';
  /* Критерий фильтра: Содержимое: Направление (field_division) */
  $handler->display->display_options['filters']['field_division_tid']['id'] = 'field_division_tid';
  $handler->display->display_options['filters']['field_division_tid']['table'] = 'field_data_field_division';
  $handler->display->display_options['filters']['field_division_tid']['field'] = 'field_division_tid';
  $handler->display->display_options['filters']['field_division_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_division_tid']['expose']['operator_id'] = 'field_division_tid_op';
  $handler->display->display_options['filters']['field_division_tid']['expose']['label'] = 'Направление';
  $handler->display->display_options['filters']['field_division_tid']['expose']['operator'] = 'field_division_tid_op';
  $handler->display->display_options['filters']['field_division_tid']['expose']['identifier'] = 'field_division_tid';
  $handler->display->display_options['filters']['field_division_tid']['expose']['remember'] = TRUE;
  $handler->display->display_options['filters']['field_division_tid']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_division_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_division_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_division_tid']['vocabulary'] = 'division';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'operators-data';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Трафик';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $translatables['rest_and_pcp'] = array(
    t('Master'),
    t('Трафик'),
    t('ещё'),
    t('Применить'),
    t('Сбросить'),
    t('Сортировать по'),
    t('По возрастанию'),
    t('По убыванию'),
    t('Выберите любой фильтр и нажмите Применить для просмотра результата'),
    t('field collection item from field_operator_traffic'),
    t('Дата'),
    t('Направление'),
    t('Ф.И.О. оператора'),
    t('Logged'),
    t('Перерыв'),
    t('% перерыва к отработанному времени'),
    t('Поствызывная обработка'),
    t('% поствызова к отработанному времени'),
    t('% загрузки'),
    t('Количество звонков'),
    t('Содержимое'),
    t('- Выберите действие -'),
    t('Page'),
  );
  $export['rest_and_pcp'] = $view;

  return $export;
}
