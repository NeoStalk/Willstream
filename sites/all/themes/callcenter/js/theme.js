(function ($) {
  Drupal.behaviors.callcenter = {
    attach : function(context, settings) {

    $(function() {
      var $Element = $('.page-client-call-detailing #files');
      $Element.width($Element.parent().width());
    });


    $(window).resize(function() {
      var $Element = $('.page-client-call-detailing #files');
      $Element.width($Element.parent().width());
    });

    $(function() {
      var $Element = $('.files-operations');
      $Element.width($Element.parent().width());
    });

    $(window).resize(function() {
      var $Element = $('.files-operations');
      $Element.width($Element.parent().width());
    });

    $(function() {
      if ($('#files-table input[type=checkbox]:checked').length > 0) {
          $("#files").show();
      }
      else {
          $("#files").hide();
      }
    });


        $("#files-table .form-checkbox").click(function() {
       if ($('#files-table table input[type=checkbox]:checked').length > 0) {
           $("#files").show();
       }
       else {
           $("#files").hide();
       }
    });


    $("#files-table .select-all.accepted").click(function(){
      $('#files-table input.accepted').not(this).prop('checked', this.checked);
    });

    $("#files-table .select-all.download").click(function(){
      $('#files-table input.download').not(this).prop('checked', this.checked);
    });



    }
  };
})(jQuery);


