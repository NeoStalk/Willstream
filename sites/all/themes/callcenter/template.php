<?php

/**
 * @file
 * The primary PHP file for this theme.
 */

/*
function callcenter_js_alter(&$js) {
    unset($js['misc/tableheader.js']);
}
*/


function callcenter_status_messages($variables){

  $display = $variables['display'];
  $output = '';

  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
  );
  foreach (drupal_get_messages($display) as $type => $messages) {
    $output .= "<div class=\"messages $type\">\n";
    if (!empty($status_heading[$type])) {
      $output .= '<h2 class="element-invisible">' . $status_heading[$type] . "</h2>\n";
    }
    if (count($messages) > 1) {
      $output .= " <ul>\n";
      foreach ($messages as $message) {
        $output .= '  <li>' . $message . "</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= reset($messages);
    }
    $output .= "</div>\n";
  }
  return $output;
}


function callcenter_tableselect($variables) {

  global $user;
  $element = $variables['element'];

  foreach ($element['#options'] as $key => $value) {
    $o = $element[$key];
    unset($element[$key]);
    $element[$key] = $o;
  }

  $rows = array();
  $header = $element['#header'];
  if (!empty($element['#options'])) {
    // Generate a table row for each selectable item in #options.
    foreach (element_children($element) as $key) {
      $row = array();
      $row['data'] = array();
      if (isset($element['#options'][$key]['#attributes'])) {
        $row += $element['#options'][$key]['#attributes'];
      }

      // Render the checkbox / radio element.
      $row['data'][] = drupal_render($element[$key]);
      // As theme_table only maps header and row columns by order, create the
      // correct order by iterating over the header fields.
      foreach ($element['#header'] as $fieldname => $title) {
        $row['data'][] = $element['#options'][$key][$fieldname];
        if ($fieldname == 'file_download') {
          $row['data'][] = drupal_render($element[$key]);
        }
      }
      $rows[] = $row;
    }

    // Add an empty header or a "Select all" checkbox to provide room for the
    // checkboxes/radios in the first table column.
    if ($element['#js_select']) {
      // Add a "Select all" checkbox.
      drupal_add_js('misc/tableselect.js');
      array_unshift($header, array(
        'class' => array(
          'select-all',
        ),
#        'data' => !in_array('клиент', $user->roles) ? '<div class="black-list-select-all">Не одобрен</div>' : '',
      ));
    }
    else {
      // Add an empty header when radio buttons are displayed or a "Select all"
      // checkbox is not desired.
      array_unshift($header, '');
    }
  }
  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'empty' => $element['#empty'],
    'attributes' => $element['#attributes'],
  ));

}